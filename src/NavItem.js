import './Header.css';

const NavItem = (props) => {
  return <div className='nav-item'>{props.value}</div>
}

export default NavItem;