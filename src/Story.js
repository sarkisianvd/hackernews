import { Component } from 'react';
import './Header.css';

class Story extends Component { 
  constructor(props) {
    super(props);
    this.state = {story: {}};
  }
  
  componentDidMount() {
    fetch(`https://hacker-news.firebaseio.com/v0/item/${this.props.value}.json?print=pretty`)
    .then(response => response.json())
    .then(data => this.setState({story: data}))
  }

  render() {
    const isLoading = this.state.story === {};
    let title = isLoading ? "Loading..." : this.state.story.title
    let score = isLoading ? "loading..." : this.state.story.score + "points"
    let by = isLoading ? "loading..." : "by " + this.state.story.by
    return (

      <div className='story' >
        <div className='story-header'>
          <div className="story-index">{this.props.index + 1 + "."}</div>
          <a className="story-title" href={this.state.story.url}> <div>{title}</div></a>
        </div>
        <div className='story-body'>
          <div>{score}</div>
          <div>{by}</div>
        </div>
      </div>
    );
  }
}

export default Story;