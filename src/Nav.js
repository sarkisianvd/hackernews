import './Header.css';
import NavItem from './NavItem';


const Nav = () => {
  let navItems = [];
  const items = ["new", "past", "comments", "ask", "show", "jobs", "submit"];
 
  let i = 0;
  while (i < items.length) { 
    navItems.push(items[i]);
    if (i < 6) {
      navItems.push("|")
    };
    i++;
  };
    
  return (
  <div className='nav'>
    {navItems.map((item, i) => <NavItem key={'nav_item'+i} value={item} />)}
  </div>
  );
}

export default Nav;
