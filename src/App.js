import Header from './Header';
import StoryContainer from './StoryContainer';

function App() {
  return (
    <div>
    <Header />
    <StoryContainer />
    </div>
  )
}

export default App;
