import './Header.css';
import Logo from './Logo';
import Nav from './Nav';

const Header = () => {
    return (
      <div className='main-header'>
        <Logo />
        <span className="hacker-tag">Hacker News</span>
        <Nav />
        <span className="space"></span>
        <span className="login">login</span>
      </div>
    )
}

export default Header;