import { Component } from 'react';
import './Header.css';
import Story from './Story.js';

class StoryContainer extends Component {
	constructor(props) {
    super(props);
    this.state = {ids: []};
  }
  
  componentDidMount() {
    fetch("https://hacker-news.firebaseio.com/v0/newstories.json?print=pretty&deleted=false")
    .then(response => response.json())
    .then(data => this.setState({ids: data.slice(-30)}))
		.catch(_error => this.setState({ids: ["error"]}))
  }

	render() {
		let content;

		if (this.state.ids.length > 1) {
			content = this.state.ids.map((item, i) => <Story key={"story_"+i} index={i} value={item} />)
		} else if (this.state.ids[0] === "error") {
			content = (<div><h3>Sorry! An error has occured..</h3></div>)
		} else {
			content = (<div><h3>Loading...</h3></div>)
		}
		return (
			<div className="storyContainer">
				{content}
			</div>
		);
	}
}

export default StoryContainer;
